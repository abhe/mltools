''' hnn: differentiable histogram modules
© Abhejit Rajagopal <abhejit.rajagopal@ucsf.edu>, 2020
'''
import os, sys, shutil, time, pdb
import numpy as np
import torch
import h5py
import matplotlib.pyplot as plt
import pytorch_lightning as pl

# def histmask(x, bins, pdf=True):
# 	''' Return boolean indices for points in x in between bins 
# 		* Works for Pytorch and Numpy
# 		If pdf==True: (PDF)
# 			then bins is used to define the bounds as: [bins[c-1], bins[c]) 
# 		Elif pdf==False: (PMF)
# 			then bins is used to define exact values
# 	'''
# 	if pdf:
# 		groups =  [((x >= bins[c-1]) * (x < bins[c])) for c in range(1,bins.shape[0])]
# 	else:
# 		groups = [(x==bins[c]) for c in range(0,bins.shape[0])]
# 	#
# 	incidence = [k.sum() for k in groups]
# 	if incidence[0].dtype==torch.int64:
# 		incidence = torch.stack(incidence).float()
# 	else:
# 		incidence = np.stack(incidence).astype(np.float64)
# 	#
# 	return groups, incidence
# ##
def histmask(x, bins, pdf=True):
	''' Return boolean indices for points in x in between bins 
		* Works for Pytorch and Numpy
		If pdf==True: (PDF)
			then bins is used to define the bounds as: [bins[c-1], bins[c]) 
		Elif pdf==False: (PMF)
			then bins is used to define exact values
	'''
	if pdf:
		groups =  [torch.where((x >= bins[c-1]) * (x < bins[c])) for c in range(1,bins.shape[0])]
	else:
		groups = [torch.where(x==bins[c]) for c in range(0,bins.shape[0])]
	#
	incidence = [k[0].shape[0] for k in groups]
	# incidence = torch.stack(incidence).float()
	return groups, incidence
##

# class histograNN(pl.LightningModule):
# 	class gaussian(pl.LightningModule):
# 		def __init__(self, µ=0.0, sigma=0.1):
# 			super().__init__()
# 			self.µ = torch.nn.Parameter(µ, requires_grad=False) #torch.tensor(µ)
# 			self.sigma = torch.nn.Parameter(sigma, requires_grad=False) #torch.tensor(sigma)
# 			self.sqrt2pi = torch.nn.Parameter(torch.tensor(np.sqrt(2.0 * np.pi)), requires_grad=False)
# 		#
# 		def forward(self, x):
# 			mag = torch.pow(self.sigma * self.sqrt2pi , -1)
# 			d = torch.exp(-0.5 * torch.pow((x - self.µ) / self.sigma, 2) )
# 			# pdb.set_trace()
# 			return mag * d
# 			return d
# 		#
# 	#
# 	def __init__(self, hmin=0.0, hmax=1.0, num_filters=10, sigma_factor=4.0):
# 		super().__init__()
# 		self.min = torch.nn.Parameter(torch.tensor(hmin).float(), requires_grad=False)
# 		self.max = torch.nn.Parameter(torch.tensor(hmax).float(), requires_grad=False)
# 		self.num_filters = num_filters
# 		self.sigma_factor = torch.nn.Parameter(torch.tensor(sigma_factor).float(), requires_grad=False)
# 		self.range = torch.nn.Parameter(self.max - self.min, requires_grad=False)
# 		self.filters = []
# 		µs = []
# 		sigmas = []
# 		for i in range(0,self.num_filters):
# 			µ = ( i/(self.num_filters-1) ) * self.range
# 			sigma = ( 1/(self.sigma_factor*(self.num_filters-1)) ) * self.range
# 			# pdb.set_trace()
# 			filt = self.gaussian( µ=µ , sigma=sigma)
# 			self.filters.append(filt)
# 			µs.append(µ)
# 			sigmas.append(sigma)
# 		#
# 		self.µs = torch.nn.Parameter(torch.stack(µs, axis=0).float(), requires_grad=False)
# 		self.sigmas = torch.nn.Parameter(torch.stack(sigmas, axis=0).float(), requires_grad=False)
# 		# w = [1/self.num_filters for i in self.µs[1:-1]]
# 		# self.weights = torch.tensor([w[0]*2] + w + [w[0]*2])
# 		self.weights = torch.nn.Parameter(torch.tensor( [1]*len(self.µs) ).float(), requires_grad=False)
# 	#
# 	def forward(self, x):
# 		histogrann = []
# 		for fi, filt in enumerate(self.filters):
# 			hval = filt(x).sum(axis=1)
# 			# pdb.set_trace()
# 			histogrann.append(hval)
# 		#
# 		assert( not self.weights.requires_grad )
# 		histogrann = torch.stack(histogrann, axis=-1) * self.weights
# 		hsum = histogrann.sum(axis=1,keepdim=True)
# 		if torch.any(hsum == 0):
# 			return histogrann
# 		histogrann = histogrann / histogrann.sum(axis=1,keepdim=True)
# 		return histogrann
# 	#
# ##

def make_GaussianFilters_uniform(hmin, hmax, num_filters, sigma_factor=2):
	hrange = np.float64(hmax - hmin)
	norm_factor = hrange / (num_filters-1)
	µs = np.arange(0, num_filters) / norm_factor
	sigma = 1 / (norm_factor * sigma_factor)
	sigmas = np.repeat(sigma, len(µs))
	return µs, sigmas
##

def eval_Gaussian(x, µ, sigma):
	scaling = 1 / (sigma * np.sqrt(2 * np.pi))
	val = scaling * np.exp( -0.5 * np.square( (x - µ) / sigma) )
	return val
##

def eval_Gaussians(x, µs, sigmas):
	scaling = 1 / (sigmas * np.sqrt(2 * np.pi))
	val = scaling * np.exp( -0.5 * np.square( (x - µs) / sigmas) )
	return val
##

def eval_filters(x, func, filter_args):
	y = []
	for args in filter_args:
		v = func(x, *list(args))
		y.append(v)
	#
	return np.concatenate(y, axis=0)
##

def plot_filters(x, y, savename=None):
	fig, axs = plt.subplots(1,1)
	axs.plot(np.tile(x,[y.shape[0],1]).T, y.T, linestyle='-')
	axs.set_xlabel('Pixel Amplitude')
	axs.set_ylabel('Frequency')
	fig.tight_layout()
	if savename is not None: fig.savefig(savename, dpi=300)
	return fig
##

class histogramNN(torch.nn.Module):
	def __init__(self, hmin=0.0, hmax=1.0, num_filters=10, sigma_factor=4.0):
		super().__init__()
		self.hmin = hmin
		self.hmax = hmax
		self.num_filters = num_filters
		self.sigma_factor = sigma_factor
		µs, sigmas = make_GaussianFilters_uniform(self.hmin, self.hmax, self.num_filters, sigma_factor=self.sigma_factor)
		# self.µs = torch.nn.Parameter(torch.tensor(µs.astype(np.float32)[:,None]), requires_grad=False)
		# self.sigmas = torch.nn.Parameter(torch.tensor(sigmas.astype(np.float32)[:,None]), requires_grad=False)
		# self.scaling = torch.nn.Parameter(1.0 / (self.sigmas * np.sqrt(2 * np.pi)), requires_grad=False)
		# self.µs = torch.tensor(µs.astype(np.float32)[:,None])
		# self.sigmas = torch.tensor(sigmas.astype(np.float32)[:,None])
		# self.scaling = 1.0 / (self.sigmas * np.sqrt(2 * np.pi))
		self.register_buffer('µs', torch.tensor(µs.astype(np.float32)[:,None]))
		self.register_buffer('sigmas', torch.tensor(sigmas.astype(np.float32)[:,None]))
		self.register_buffer('scaling', 1.0 / (self.sigmas * np.sqrt(2 * np.pi)))
		# self.µs = self._buffers['µs']
		# self.sigmas = self._buffers['sigmas']
		# self.scaling = self._buffers['scaling']
		# pdb.set_trace()
	#
	def eval_gaussians(self, x):
		# return self.scaling * torch.exp( -0.5 * torch.square( (x - self.µs) / self.sigmas) )
		return self._buffers['scaling'] * torch.exp( -0.5 * torch.square( (x - self._buffers['µs']) / self._buffers['sigmas']) )
	##
	def forward(self, x, normalize=True):
		rbf_eval = self.eval_gaussians(x)
		# pdb.set_trace()
		hist_rbf = rbf_eval.sum(axis=1, keepdim=True).T
		hist_std = rbf_eval.std(axis=1, keepdim=True).T
		if normalize:
			# hist_rbf = torch.nn.functional.softmax(hist_rbf,dim=1)
			# pass
			# hist_rbf = hist_rbf / hist_rbf.sum(axis=1,keepdims=True)
			# hist_rbf = hist_rbf / x.sum(axis=1,keepdims=True)
			hist_rbf = hist_rbf / (torch.isfinite(x)).float().sum(axis=1,keepdims=True)
		#
		return hist_rbf, hist_std
	#
##	

if __name__=='__main__':
	hmin = 0.0
	hmax = 5.0
	num_filters = 6
	sigma_factor = 3

	## Filter test/plot
	x = np.linspace(-1,6,num=200, dtype=np.float32)[None,:]
	µs, sigmas = make_GaussianFilters_uniform(hmin, hmax, 6, sigma_factor=sigma_factor)
	y_slow = eval_filters(x, eval_Gaussian, zip(µs, sigmas))
	y_fast = eval_Gaussians(x, µs[:,None], sigmas[:,None])
	assert(np.allclose(y_slow,y_fast))
	x_tensor = torch.tensor(x)
	hnn = histogramNN(hmin=hmin, hmax=hmax, num_filters=num_filters, sigma_factor=sigma_factor)
	y_tensor = hnn.eval_gaussians(x_tensor)
	y_torch = y_tensor.to("cpu").detach().numpy()
	y = y_torch
	fig = plot_filters(x, y, 'filt6.png')

	## Opt test.
	# x = np.linspace(0,5,num=100, dtype=np.float32)[None,:]
	x = np.linspace(-2,2,num=100, dtype=np.float32)[None,:]
	x_tanh = x.copy()
	# x_tanh = ((torch.tanh(torch.tensor(x)) + 1) * 5/2).numpy()
	print('orig: ', x.min(), x.max(), flush=True)
	print('after tanh: ', x_tanh.min(), x_tanh.max(), flush=True)
	hist_rbf, hist_rbf_std = hnn(torch.tensor(x_tanh))
	hist_rbf_torch = hist_rbf.to("cpu").detach().numpy()
	print('orig RBF: ', hist_rbf, flush=True)
	print('orig RBF std: ', hist_rbf_std, flush=True)
	
	x_tensor_param = torch.nn.Parameter(torch.tensor(x), requires_grad=True)
	# target_hist = np.asarray([0.1, 0.6, 0.2, 0.1, 0, 0],dtype=np.float32)[None,:]
	target_hist = np.asarray([0.0, 0.1, 0.5, 0.4, 0.0, 0],dtype=np.float32)[None,:]
	# target_hist = np.asarray([0.0, 0.0, 1, 0, 0, 0],dtype=np.float32)[None,:]
	target_hist_tensor = torch.tensor(target_hist)

	num_iters = 100000
	optimizer = torch.optim.Adam([x_tensor_param], lr=0.0001, weight_decay=0.0)
	# optimizer = torch.optim.AdamW([x_tensor_param], lr=0.0001, weight_decay=0.0)
	# optimizer = torch.optim.SGD([x_tensor_param], lr=0.01)
	for step in range(0,num_iters):
		# y_grading = (torch.tanh(x_tensor_param)+1)*(5/2)
		y_grading = x_tensor_param
		pred_hist, pred_hist_std = hnn(y_grading)
		loss = torch.square(pred_hist - target_hist_tensor).mean()
		# loss = loss + torch.mean(torch.abs(pred_hist_std))
		loss += torch.square((torch.relu(y_grading - 5.1) + torch.relu(-0.1 - y_grading))).mean()
		# pdb.set_trace()
		loss.backward()
		optimizer.step()
		loss_np = loss.to("cpu").detach().numpy()
		grad_np, *_ = x_tensor_param.grad.data
		if step%(num_iters//10)==0: print('Step:{:03d}, loss={:2.4f}, '.format(step, loss_np), np.linalg.norm(grad_np), flush=True)
		# pdb.set_trace()
	#
	x_tanh_final = y_grading.to("cpu").detach().numpy()
	# x_tanh_final = np.sort(x_tanh_final)
	print('V:\n',np.vstack([x_tanh,x_tanh_final]).T)
	pred_final = pred_hist.to("cpu").detach().numpy()
	cmp_OPTD = np.concatenate([hist_rbf_torch, pred_final, target_hist],axis=0)
	print('HNN:\n',cmp_OPTD.round(3))

	fig, ax = plt.subplots(1,1, figsize=(5,5))
	ax.plot(hnn._buffers['µs'].squeeze(), hist_rbf_torch.squeeze(), label='original distribution')
	ax.plot(hnn._buffers['µs'].squeeze(), target_hist.squeeze(), label='target distribution')
	ax.plot(hnn._buffers['µs'].squeeze(), pred_final.squeeze(), label='new distribution')
	ax.set_xlim([-1,6])
	ax.legend(loc='best')
	ax.set_title('When #iters={}'.format(num_iters))
	savename = 'hplot_{}.png'.format(num_iters)
	os.remove(savename)
	fig.savefig(savename,dpi=200)


	# hbins_orig = np.arange(-0.5, 6.5, 1.0)
	# hbins = np.asarray([-5, -1, *hbins_orig, 6, 10])
	# orig_hist = np.histogram(x_tanh, bins=hbins, density=True)[0].round(3)
	# new_hist = np.histogram(x_tanh_final, bins=hbins, density=True)[0].round(3)
	# print('HIST:\n',np.stack([orig_hist, new_hist],axis=0))

	# fig, ax = plt.subplots(1,1, figsize=(5,5))
	# ax.plot(hbins[:-1]+0.5, orig_hist, label='original distribution')
	# ax.plot(hbins_orig[:-1]+0.5, target_hist.squeeze(), label='target distribution')
	# ax.plot(hbins[:-1]+0.5, new_hist, label='new distribution')
	# ax.set_xlim([-5,10])
	# ax.legend(loc='best')
	# ax.set_title('When #iters={}'.format(num_iters))
	# fig.savefig('hplot_{}.png'.format(num_iters),dpi=200)
#

##

# if __name__=='__main__':

# 	## histograNN (torch)
# 	# x = np.linspace(0.0, 1.0, num=200, dtype=np.float32)
# 	x = np.random.random(250).astype(np.float32)
# 	x_torch = torch.tensor(x[None,...])
# 	HNN = histograNN(num_filters=20, sigma_factor=3)
# 	yhat = HNN(x_torch)

# 	## histogram (numpy)
# 	µs = HNN.µs.numpy()
# 	w = (µs[1:] - µs[:-1])/2
# 	bins = [µs[0], *(µs[:-1]+w), µs[-1]]
# 	h = np.histogram(x, bins)[0]
# 	h = h / h.sum()

# 	# numpy gaussian windows for plotting
# 	def make_gaussian(µ=0.0, sigma=0.1):
# 		# mag = np.power(sigma * np.sqrt(2 * np.pi), -1)
# 		# return lambda x: mag * np.exp(-0.5 * np.power((x - µ) / sigma, 2) )
# 		return lambda x: np.exp(-0.5 * np.power((x - µ) / sigma, 2) )
# 	#
# 	num_filts = µs.shape[0]
# 	sigma_factor = HNN.sigma_factor.to("cpu").numpy()
# 	filts = []
# 	for i in range(0,num_filts):
# 		filt = make_gaussian(i/(num_filts-1), sigma=1/(sigma_factor*(num_filts-1)))
# 		filts.append(filt)
# 	#
# 	x0 = np.linspace(0.0, 1.0, num=200)
# 	fval = np.asarray([f(x0) for f in filts]).T
# 	fval = fval / fval.sum(axis=0) #[...,None]#[None,...]
# 	fval[:,0] = fval[:,0]/2
# 	fval[:,-1] = fval[:,-1]/2

# 	## plotting
# 	f, axs = plt.subplots(1,1)
# 	axs.plot(x0,fval, linestyle='-')
# 	axs.scatter(HNN.µs, yhat[0], color='black', s=50)
# 	axs.scatter(HNN.µs, h , color='green', s=5)
# 	axs.set_xlabel('Pixel Amplitude')
# 	axs.set_ylabel('Frequency')
# 	f.tight_layout()
# 	f.savefig('filts.png')
# 	plt.show()




