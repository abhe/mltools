''' torchutiils: helper functions for 2D/3D subvolume indexing, custom sampling, + more
© Abhejit Rajagopal <abhejit.rajagopal@ucsf.edu>, 2020
'''
import os, sys, time, random, itertools, glob, pdb
from tqdm import tqdm
import numpy as np
import h5py
import pandas as pd
import torch
from collections import defaultdict, OrderedDict

import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid

# import mltools.pytorchSSIM3D.pytorch_ssim as pytorchSSIM3D
from . import pytorch_ssim as pytorchSSIM3D

#FedSGD
from typing import Callable, Iterable, Optional, Tuple, Union
class FedSGD(torch.optim.Optimizer):
	"""
	Implements Adam algorithm with weight decay fix as introduced in [Decoupled Weight Decay
	Regularization](https://arxiv.org/abs/1711.05101).

	Parameters:
		params (`Iterable[torch.nn.parameter.Parameter]`):
			Iterable of parameters to optimize or dictionaries defining parameter groups.
		lr (`float`, *optional*, defaults to 1e-3):
			The learning rate to use.
		socket
		token
	"""

	def __init__(
		self,
		params: Iterable[torch.nn.parameter.Parameter],
		lr: float = 1e-3,
		socket: str = '127.0.0.1:7771',
		token: str = 'secret'
	):
		if lr < 0.0:
			raise ValueError(f"Invalid learning rate: {lr} - should be >= 0.0")
		defaults = dict(lr=lr, socket=socket, token=token)
		super().__init__(params, defaults)

	def step(self, closure: Callable = None):
		"""
		Performs a single optimization step.

		Arguments:
			closure (`Callable`, *optional*): A closure that reevaluates the model and returns the loss.
		"""
		loss = None
		if closure is not None:
			loss = closure()

		pdb.set_trace()
		for group in self.param_groups:
			for p in group["params"]:
				if p.grad is None:
					continue
				grad = p.grad.data
				if grad.is_sparse:
					raise RuntimeError("Adam does not support sparse gradients, please consider SparseAdam instead")

				state = self.state[p]

				# State initialization
				if len(state) == 0:
					state["step"] = 0
					# Exponential moving average of gradient values
					state["exp_avg"] = torch.zeros_like(p.data)
					# Exponential moving average of squared gradient values
					state["exp_avg_sq"] = torch.zeros_like(p.data)

				exp_avg, exp_avg_sq = state["exp_avg"], state["exp_avg_sq"]
				beta1, beta2 = group["betas"]

				state["step"] += 1

				# Decay the first and second moment running average coefficient
				# In-place operations to update the averages at the same time
				exp_avg.mul_(beta1).add_(grad, alpha=(1.0 - beta1))
				exp_avg_sq.mul_(beta2).addcmul_(grad, grad, value=1.0 - beta2)
				denom = exp_avg_sq.sqrt().add_(group["eps"])

				step_size = group["lr"]
				if group["correct_bias"]:  # No bias correction for Bert
					bias_correction1 = 1.0 - beta1 ** state["step"]
					bias_correction2 = 1.0 - beta2 ** state["step"]
					step_size = step_size * math.sqrt(bias_correction2) / bias_correction1

				p.data.addcdiv_(exp_avg, denom, value=-step_size)

				# Just adding the square of the weights to the loss function is *not*
				# the correct way of using L2 regularization/weight decay with Adam,
				# since that will interact with the m and v parameters in strange ways.
				#
				# Instead we want to decay the weights in a manner that doesn't interact
				# with the m/v parameters. This is equivalent to adding the square
				# of the weights to the loss with plain (non-momentum) SGD.
				# Add weight decay at the end (fixed version)
				if group["weight_decay"] > 0.0:
					p.data.add_(p.data, alpha=(-group["lr"] * group["weight_decay"]))

		return loss
	#
##



# sample ND subvolumes.
def gen_SubVolumes(sample, vshape, volsize, stride, keep_keys, copy_keys=['file','idx'], randomize=False, check_size=True, accept_criteria=None, bbox_overlap=None, doubletrouble=True, debug=False):
	''' Python generator returning all possible multi-channel 3-dim subvolumes
		--> How to generalize for N-dim subvolumes?

		Inputs:
			sample: -- a dictionary, with values being multichannel 3D tensors
			vshape:	-- the spatial dimensions of the 3D tensor(s) (can we infer this sample['some_key'].shape ?)
			dimensions: -- the indices to loop over (can we infer this from stride?)
			volsize: -- the target subvolume spatial size/shape
			stride: -- stride in each dimension to move volsize window
			keep_keys: -- which keys to actually index into / crop
			copy_keys: -- which keys to copy direct from sample
			subsample: -- possibly pre-populated dictionary, to add stuff to. (e.g. pre-load dict with 'file')

		Outputs:
			generator: -- subvolume sample dictionary with keys 'idx', and any others of interest.
	'''
	assert(len(vshape) == len(volsize) and len(volsize)==len(stride))
	dimensions = len(stride)
	if debug: print(vshape, stride, volsize)

	# find all valid start-end indices in each dimension
	idxlists = []
	for dim in range(0,dimensions):
		if debug: print(vshape[dim], volsize[dim], stride[dim])
		i_st = np.arange(0,vshape[dim] - volsize[dim] + 1,stride[dim])
		i_nd = np.clip(i_st + volsize[dim],0,vshape[dim])
		v1 = list(zip(i_st, i_nd))
		if not doubletrouble:
			idxlists.append( v1 )
		else:
			i_nd = np.arange(vshape[dim], volsize[dim] -1, -1*stride[dim])
			i_st = np.clip(i_nd - volsize[dim],0,vshape[dim])
			v2 = list(zip(i_st, i_nd))[::-1]
			v12 = sorted(list(set(v1) | set(v2)))
			idxlists.append( v12 )
		#
		# # print(dim)
		# # print(i_st)
		# # print(i_nd)
	#
	if debug:
		# print(idxlists)
		print(len(idxlists), [len(k) for k in idxlists])
	#
	# pdb.set_trace()

	# mix these up randomly
	if randomize:
		for dim in range(len(idxlists)):
			random.shuffle(idxlists[dim])
		#
		# pdb.set_trace()
	#

	# generate all possible subvolumes~
	# indices = itertools.product(*idxlists)
	indices = list(itertools.product(*idxlists))
	random.shuffle(indices)
	for idxs in indices:
		subsample = {}
		subsample['idx'] = np.asarray(idxs) #np.expand_dims(idxs,axis=0)
		ix, iy, iz = idxs
		if bbox_overlap is not None:
			if not bbox_complete_overlap(bbox_overlap, idxs):
				continue
		for k in copy_keys:
			if k in sample.keys(): subsample[k] = sample[k]
		# print(ix, iy, iz, flush=True)
		# ssample = { 'file': path, 'idx': np.expand_dims((ix, iy, iz),axis=0) }
		for k in keep_keys:
			if k in sample.keys():
				subsample[k] = sample[k][...,ix[0]:ix[1],iy[0]:iy[1],iz[0]:iz[1]].copy()
				checksamp_k = k
			# ssample[k] = sample[k][(Ellipsis,*idxs)]
			# print(k, subsample[k].shape, sample[k].shape)
			# pdb.set_trace()
		#
		if check_size and not np.all(np.asarray(subsample[checksamp_k].shape[1:]) == np.asarray(volsize)):
			# del subsample # deleting conflicts with passing subsample in
			pdb.set_trace()
			continue
		#
		if accept_criteria is not None and not accept_criteria(subsample):
			# print(((subsample['Lesion_Masks']>0) + (subsample['Sextant_Masks']>0)).astype(np.int64).sum(),flush=True)
			# print( (subsample['Lesion_Masks']>0).astype(np.int64).sum(), (subsample['Sextant_Masks']>0).astype(np.int64).sum(), flush=True)
			# print('*',end='',flush=True)
			# pdb.set_trace()
			continue
		#
		# pdb.set_trace()
		yield subsample
	#
##

def gen_SubVolumes2D(sample, vshape, volsize, stride, keep_keys, copy_keys=['file','idx'], randomize=False, check_size=True, accept_criteria=None, bbox_overlap=None, doubletrouble=True, debug=False):
	''' Python generator returning all possible multi-channel 3-dim subvolumes
		--> How to generalize for N-dim subvolumes?

		Inputs:
			sample: -- a dictionary, with values being multichannel 3D tensors
			vshape:	-- the spatial dimensions of the 3D tensor(s) (can we infer this sample['some_key'].shape ?)
			dimensions: -- the indices to loop over (can we infer this from stride?)
			volsize: -- the target subvolume spatial size/shape
			stride: -- stride in each dimension to move volsize window
			keep_keys: -- which keys to actually index into / crop
			copy_keys: -- which keys to copy direct from sample
			subsample: -- possibly pre-populated dictionary, to add stuff to. (e.g. pre-load dict with 'file')

		Outputs:
			generator: -- subvolume sample dictionary with keys 'idx', and any others of interest.
	'''
	assert(len(vshape) == len(volsize) and len(volsize)==len(stride))
	dimensions = len(stride)
	if debug: print(vshape, stride, volsize)

	# find all valid start-end indices in each dimension
	idxlists = []
	for dim in range(0,dimensions):
		if debug: print(vshape[dim], volsize[dim], stride[dim])
		i_st = np.arange(0,vshape[dim] - volsize[dim] + 1,stride[dim])
		i_nd = np.clip(i_st + volsize[dim],0,vshape[dim])
		v1 = list(zip(i_st, i_nd))
		if not doubletrouble:
			idxlists.append( v1 )
		else:
			i_nd = np.arange(vshape[dim], volsize[dim] -1, -1*stride[dim])
			i_st = np.clip(i_nd - volsize[dim],0,vshape[dim])
			v2 = list(zip(i_st, i_nd))[::-1]
			v12 = sorted(list(set(v1) | set(v2)))
			idxlists.append( v12 )
		#
		# # print(dim)
		# # print(i_st)
		# # print(i_nd)
	#
	if debug:
		# print(idxlists)
		print(len(idxlists), [len(k) for k in idxlists])
	#
	# pdb.set_trace()

	# mix these up randomly
	if randomize:
		for dim in range(len(idxlists)):
			random.shuffle(idxlists[dim])
		#
		# pdb.set_trace()
	#

	# generate all possible subvolumes~
	# indices = itertools.product(*idxlists)
	indices = list(itertools.product(*idxlists))
	random.shuffle(indices)
	for idxs in indices:
		subsample = {}
		subsample['idx'] = np.asarray(idxs) #np.expand_dims(idxs,axis=0)
		ix, iy = idxs
		if bbox_overlap is not None:
			if not bbox_complete_overlap(bbox_overlap, idxs):
				continue
		for k in copy_keys:
			if k in sample.keys(): subsample[k] = sample[k]
		# print(ix, iy, iz, flush=True)
		# ssample = { 'file': path, 'idx': np.expand_dims((ix, iy, iz),axis=0) }
		for k in keep_keys:
			if k in sample.keys():
				subsample[k] = sample[k][...,ix[0]:ix[1],iy[0]:iy[1]].copy()
				checksamp_k = k
			# ssample[k] = sample[k][(Ellipsis,*idxs)]
			# print(k, subsample[k].shape, sample[k].shape)
			# pdb.set_trace()
		#
		if check_size and not np.all(np.asarray(subsample[checksamp_k].shape[1:]) == np.asarray(volsize)):
			# del subsample # deleting conflicts with passing subsample in
			pdb.set_trace()
			continue
		#
		if accept_criteria is not None and not accept_criteria(subsample):
			# print(((subsample['Lesion_Masks']>0) + (subsample['Sextant_Masks']>0)).astype(np.int64).sum(),flush=True)
			# print( (subsample['Lesion_Masks']>0).astype(np.int64).sum(), (subsample['Sextant_Masks']>0).astype(np.int64).sum(), flush=True)
			# print('*',end='',flush=True)
			# pdb.set_trace()
			continue
		#
		# pdb.set_trace()
		yield subsample
	#
##

def assemble3D_mean(idx_list, vol_lists, **kwargs):
	'''
		Assemble 3D volumes from lists of chunks, with corresponding 3D slice indices.
	'''
	max_idx = idx_list.max(axis=(0,2))
	min_idx = idx_list.min(axis=(0,2))
	vol_full = [np.zeros( [x.shape[1], *max_idx], dtype=np.float32) for x in vol_lists]
	count_full = np.zeros(vol_full[0].shape[1:], dtype=np.int32)
	for idx,subvols in zip(idx_list, zip(*vol_lists)):
		for volidx in range(len(subvols)):
			vol_full[volidx][:,idx[0][0]:idx[0][1],idx[1][0]:idx[1][1], idx[2][0]:idx[2][1]] += subvols[volidx]
		#
		count_full[idx[0][0]:idx[0][1],idx[1][0]:idx[1][1],idx[2][0]:idx[2][1]] += 1
	#
	return [vol / np.expand_dims(count_full,axis=0) for vol in vol_full], count_full
##

def assemble2D_mean(idx_list, vol_lists, **kwargs):
	'''
		Assemble 2D volumes from lists of chunks, with corresponding 2D slice indices.
	'''
	max_idx = idx_list.max(axis=(0,2))
	min_idx = idx_list.min(axis=(0,2))
	vol_full = [np.zeros( [*x.shape[1:-2], *max_idx], dtype=np.float32) for x in vol_lists]
	count_full = np.zeros(vol_full[0].shape[-2:], dtype=np.int32)
	for idx,subvols in zip(idx_list, zip(*vol_lists)):
		for volidx in range(len(subvols)):
			vol_full[volidx][...,idx[0][0]:idx[0][1],idx[1][0]:idx[1][1]] += subvols[volidx]
		#
		count_full[idx[0][0]:idx[0][1],idx[1][0]:idx[1][1]] += 1
	#
	return [(vol / np.expand_dims(count_full,axis=0)).squeeze() for vol in vol_full], count_full
##

def qaplot(tensors, batched=False, sliceZ='mid', cmaps=None, titles=None, savename=None):
	'''
		tensors = list of tensors
		tensor = shape: (N, C, H, W, D) == (batchsize, # channels, height, width, depth)

		Makes a plot with each item in batch as 1 row:
			for i in range(len(tensors)):
				for c in range(C):
					plot[ col_ic ] = tensors[i][n,c,:,:,z]   )
				#
			#
	'''
	if not batched:
		tensors = [np.expand_dims(t,axis=0) for t in tensors]
	#
	nrows = tensors[0].shape[0]
	ncols = sum([t.shape[1] for t in tensors])
	if str(sliceZ)=='mid':
		sliceZ = tensors[0].shape[-1]//2
	#
	if cmaps is None:
		cmaps = [ ['jet' for c in range(t.shape[1])] for i,t in enumerate(tensors) ]
	elif type(cmaps)==type(''):
		cmaps = [ [cmaps for c in range(t.shape[1])] for i,t in enumerate(tensors) ]
	#
	assert([len(c)==t.shape[1] for c,t in zip(cmaps, tensors)])
	if titles is None:
		titles = [ ['Vol {}, Channel {}'.format(i,c) for c in range(t.shape[1])] for i,t in enumerate(tensors) ]
	#
	assert([len(c)==t.shape[1] for c,t in zip(titles, tensors)])
	fig = plt.figure(figsize=(ncols*3,nrows*3))
	ax = ImageGrid(fig, 111, nrows_ncols=(nrows,ncols), axes_pad=0.05, share_all=True)

	for n in range(nrows):
		j = 0
		for i in range(len(tensors)):
			for c, tensor_ic in enumerate(tensors[i][n]): # this is the i'th volume of the nth tensor
				ax[n*ncols + j].imshow( tensor_ic[:,:,sliceZ], cmap=cmaps[i][c], interpolation='none')

				if n==0:
					ax[n*ncols + j].set_title(titles[i][c])
				#

				j +=1
			#
		#
	#
	fig.tight_layout(h_pad=1)
	if savename is not None: fig.savefig(savename)
	return fig
##

def bbox3D(vol3D, N=3):
	valid = np.argwhere(vol3D)
	axis_mins = valid.min(axis=0)
	axis_maxs = valid.max(axis=0)
	stacked = np.stack([axis_mins,axis_maxs],axis=1)
	return tuple(stacked.tolist())
##

def add_bboxbuffer(bbox, buffer=[0,0,0], max_shape=None, min_shape=None):
	if max_shape is None: max_shape = tuple([bbox[k][1]+buffer[k] for k in range(len(bbox))])
	if min_shape is None: min_shape = tuple([0 for k in range(len(bbox))])
	return tuple([ (max(dim[0] - buffer[d]//2, min_shape[d]), min(dim[1] + buffer[d]//2, max_shape[d])) for d,dim in enumerate(bbox) ] )
#

def crop3D(vol3D, coords):
	return vol3D[...,coords[0][0]:coords[0][1]+1, coords[1][0]:coords[1][1]+1, coords[2][0]:coords[2][1]+1]
##

def bbox_complete_overlap(bbox_small, bbox_large):
	return all([kL[0] <= kS[0] <= kL[1] and kL[0] <= kS[1] <= kL[1] for kS, kL in zip(bbox_small, bbox_large)])
##

# batch sampling across patient exams
def roundrobin(*iterables):
	"roundrobin('ABC', 'D', 'EF') --> A D E B F C"
	# Recipe credited to George Sakkis
	num_active = len(iterables)
	nexts = itertools.cycle(iter(it).__next__ for it in iterables)
	while num_active:
		try:
			for next in nexts:
				yield next()
			#
		#
		except StopIteration:
			# Remove the iterator we just exhausted from the cycle.
			num_active -= 1
			nexts = itertools.cycle(itertools.islice(nexts, num_active))
		#
	#
##

def groups2h5(groups, _trainidxfile):
	hf = pd.HDFStore(_trainidxfile, mode='a', complevel=9, complib='bzip2')
	for k,v in groups.items():
		hf['{}'.format(k)] = pd.DataFrame(v)
	hf.close()
	del hf, groups
	return 1
##

def hdfstore2dict(store,group,keys=None, substr='', debug=0):
	print('Loading H5 file {}...'.format(store),flush=True)
	hf = pd.HDFStore(store, mode='r')
	# grouping = {k.split(group+'/')[-1]:hf[k][:] for k in hf.keys() if group+'/'+substr in k}
	grouping = OrderedDict()
	ikeys = keys if keys is not None else hf.keys()
	for k in ikeys:
		key = k.split(group+'/')[-1]
		if debug: print(key, flush=True)
		if keys is not None or group+'/'+substr in k:
			v = hf[k][:]
			if debug: print(key, v.shape, flush=True)
			grouping[key] = v
		#
	hf.close()
	return grouping
##

def pd2gen(frame, idxs):
	for i in idxs:
		tup = list(frame[i : i+1 ].values[0][0:2])
		# tup[-1] = tup[-1][0]
		tup = tuple(tup)
		yield tup
	#
#

def load_catalog_groups(groups, reader, reader_kwargs, resample='min', batchsize=2, num_workers=14, pin_memory=True, print_groups=True):
	train_sampler = dictSampler(groups, resample=resample)
	for k,v in groups.items():
		if print_groups: print('\t', k, v.shape,flush=True)
	#
	ds_train = SubVolumes(len(train_sampler), reader, **reader_kwargs)
	dl_train = torch.utils.data.DataLoader(ds_train, pin_memory=pin_memory, num_workers=num_workers, batch_size=batchsize, sampler=train_sampler)
	count_train = ds_train.num_examples // batchsize
	print('--> # steps in epoch:', count_train,flush=True)
	return ds_train, dl_train, count_train
##

def load_traindata_groups(groups, reader, resample='min', batchsize=2, num_workers=14, pin_memory=True, print_groups=True, **reader_kwargs):
	train_sampler = dictSampler(groups, resample=resample)
	for k,v in groups.items():
		if print_groups: print('\t', k, v.shape,flush=True)
	#
	ds_train = SubVolumes(len(train_sampler), reader, **reader_kwargs)
	dl_train = torch.utils.data.DataLoader(ds_train, pin_memory=pin_memory, num_workers=num_workers, batch_size=batchsize, sampler=train_sampler)
	count_train = ds_train.num_examples // batchsize
	print('--> # steps in epoch:', count_train,flush=True)
	return ds_train, dl_train, count_train
##

def load_model(model, loadfile, overwrite=False, **kwargs):
	if overwrite or not os.path.exists(loadfile):
		print('Clean model...')
		model = model(**kwargs)
	else:
		print('Loading model from checkpoint: {}'.format(loadfile),flush=True)
		model = model.load_from_checkpoint(loadfile, strict=False, **kwargs)
	#
	return model
##

def find_best_model(dir_checkpoints, dir_tensorboard, metrics_to_track=None):
	''' SHOULD DEPEND ON metrics_to_track -- need to implement'''
	import tensorflow as tf
	from tensorflow.python.summary.summary_iterator import summary_iterator
	model_checkpoints = sorted(glob.glob(dir_checkpoints+'/*.ckpt'))
	saved_epochs = [int(k.split('epoch=')[-1].split('-')[0]) for k in model_checkpoints if 'epoch' in k]
	event_files = [k for k in glob.glob(dir_tensorboard+'/**/*') if 'events.out.tfevents' in k]
	# event_file = event_files[0]
	def read_eventfile(event_file, onlyval=True, dropTrain=True, metrics_to_track=None):
		logdata = defaultdict(OrderedDict)
		for e in summary_iterator(event_file):
			if len(e.summary.value)==0: continue
			# tmp = {}
			# tmp['step'] = e.step
			logdata[e.step]['step'] = e.step
			for v in e.summary.value:
				if any([k in v.tag for k in ['/']]): continue
				if metrics_to_track is not None:
					if not v in metrics_to_track: continue
				#

				logdata[e.step][v.tag] = v.simple_value
				# print(e.step, v.tag, v.simple_value)
				# tmp[v.tag] = v.simple_value
				# stuff[e.step] = e.summary.value
				# stuff[]
				# break
			#
			# if 'epoch' not in tmp.keys(): continue
			# if not any(['val' in k for k in tmp.keys()]): continue
			# logdata.append(tmp)
		#
		if onlyval: logdata = {k:v for k,v in logdata.items() if any(['val' in thekey for thekey in v.keys()])}
		log_df = pd.DataFrame(logdata).T
		if dropTrain: log_df = log_df.drop([k for k in log_df.keys() if 'train' in k],1)
		return log_df
	##
	event_data = pd.concat([read_eventfile(event_file, onlyval=True) for event_file in tqdm(event_files)]).reset_index().drop(['step','index'],1)
	valid_rows = np.asarray([np.where(event_data['epoch']==k)[0][0] for k in saved_epochs])
	event_data = event_data.loc[valid_rows]
	if metrics_to_track is None:
		event_data_simple = event_data.drop([k for k in event_data.keys() if not any([k2 in k for k2 in ['IoU','acc']])],1).reset_index().drop('index',1)
	else:
		event_data_simple = event_data
	#
	best_epoch_idx = event_data_simple.values.prod(axis=-1).argmax()
	best_epoch = int(event_data['epoch'].values[best_epoch_idx])
	best_epoch_stats = event_data.loc[event_data['epoch']==best_epoch]
	# best_checkpoint = [k for k in model_checkpoints if 'epoch={:d}'.format(best_epoch) in k][0]
	best_checkpoint = [k for k in model_checkpoints if 'epoch='+f'{best_epoch:04d}' in k][0]
	
	# print(event_data_simple)
	return best_checkpoint, best_epoch_stats
##

## pytorch multi-readers
class dictSampler(torch.utils.data.sampler.Sampler):
	def __init__(self, groupdict, balance=True, method='equal', resample='max', shuffle=True):
		self.groupdict = groupdict
		self.num_examples = [v.shape[0] for k,v in self.groupdict.items()]
		# if resample=='max':
		# 	self.max_examples = max(self.num_examples)
		# 	self.num2get = self.num_examples if balance is False else [self.max_examples for k in self.num_examples]
		# 	self.idxlist = [np.random.choice(np.arange(self.num2get[ki]),size=self.num2get[ki],replace=False) % self.num_examples[ki] for ki in range(len(self.groupdict.keys())) ]
		# elif resample=='min': # this would only work if we generated a new idxlist every epoch, otherwise this just throws out all extra data!
		# 	self.max_examples = min(self.num_examples)
		# 	self.num2get = self.num_examples if balance is False else [self.max_examples for k in self.num_examples]
		# 	self.idxlist = [np.random.choice(np.arange(self.num_examples[ki]),size=self.num2get[ki],replace=False) for ki in range(len(self.groupdict.keys())) ]
		# else:
		# 	raise(NotImplementedError)
		# #
		self.idxlist = [np.random.choice(np.arange(self.num_examples[ki]),size=self.num_examples[ki],replace=False) for ki in range(len(self.groupdict.keys())) ]
		self.gens = [itertools.cycle(pd2gen(self.groupdict[k], self.idxlist[ki])) for ki,k in enumerate(self.groupdict.keys())]
		self.gen = roundrobin(*self.gens)
		if resample=='max':
			self.length = max(self.num_examples) * len(self.num_examples)
		elif resample=='min':
			self.length = min(self.num_examples) * len(self.num_examples)
		else:
			self.length = sum(self.num_examples)
			raise(NotImplementedError)
		#
	##
	def __len__(self):
		return self.length
	##
	def __iter__(self):
		return self.gen
	##
##
class SubVolumes(torch.utils.data.Dataset):
	''' A PyTorch Dataset that provides access to subvolumes. '''
	def __init__(self, num_examples, reader, **reader_kwargs):
		"""
		Args:
			num_examples: # of items in the dataset (for batchsing/epochs)
			reader: function to load / return a single patch, with arguments generated by a custom Sampler (may not just be indices!)
			reader_kwargs: kwargs to supply to reader
		"""
		self.num_examples = num_examples
		self.reader = reader
		self.reader_kwargs = reader_kwargs
	##
	def __len__(self):
		return self.num_examples
	##
	def __getitem__(self, args):
		# print('GETITEM:', len(args), type(args), *args)
		return self.reader(*args, **self.reader_kwargs)
	#
##

class SubVolumes_iter(torch.utils.data.Dataset):
	''' A PyTorch Dataset that provides access to subvolumes. '''
	def __init__(self, groupdict, reader, *reader_args, **reader_kwargs):
		"""
		Args:
			num_examples: # of items in the dataset (for batchsing/epochs)
			reader: function to load / return a single patch, with arguments generated by a custom Sampler (may not just be indices!)
			reader_kwargs: kwargs to supply to reader
		"""
		self.groupdict = groupdict
		self.reader = reader
		self.reader_args = reader_args
		self.reader_kwargs = reader_kwargs

		self.num_examples = [v.shape[0] for k,v in self.groupdict.items()]
		self.max_examples = max(self.num_examples)

		self.makegen()
	##
	def makegen(self):
		self.num2get = [self.max_examples for k in self.num_examples]
		self.idxlist = [np.random.choice(np.arange(self.num2get[ki]),size=self.num2get[ki],replace=False) % self.num_examples[ki] for ki in range(len(self.groupdict.keys())) ]
		self.gens = [pd2gen(self.groupdict[k], self.idxlist[ki]) for ki,k in enumerate(self.groupdict.keys())]
		self.gen = roundrobin(*self.gens)
	##
	def __len__(self):
		return sum(self.num2get)
	##
	# def __getitem__(self, args):
	# 	print('GETITEM:', len(args), type(args), *args)
	# 	return self.reader(*args, **self.reader_kwargs)
	# ##
	def __iter__(self):
		# return self.__getitem__(next(self.gen))
		for t in self.gen:
			# yield self.__getitem__(t)
			# print(*t)
			stuff = self.reader(*t, **self.reader_kwargs)
			print(stuff['file'], stuff['idx'].tolist())
			yield stuff 
		#
	##
##
class simple_dataset(torch.utils.data.Dataset):
	def __init__(self, h5filelist, train=False, printkwargs=True, reader=None, **reader_kwargs):
		'''
			Args:
				h5filelist (list): list of filepaths to train HDF5 files
		'''
		self.files = h5filelist
		self.train = train
		self.reader = reader
		self.reader_kwargs = reader_kwargs
		if printkwargs: print(self.reader, self.reader_kwargs)
		if self.train:
			random.shuffle(self.files)
		#
		# raise()
	##
	def __len__(self):
		return len(self.files)
	#
	def __getitem__(self,idx):
		f = self.files[idx]
		items = self.reader(f, **self.reader_kwargs)
		return items
	#
##
class iterable_dataset(torch.utils.data.IterableDataset):
	def __init__(self, h5filelist, train=False, printkwargs=True, reader=None, **reader_kwargs):
		'''
			Args:
				h5filelist (list): list of filepaths to train HDF5 files
		'''
		self.files = h5filelist
		self.train = train
		self.reader = reader
		self.reader_kwargs = reader_kwargs
		if printkwargs: print(self.reader, self.reader_kwargs)
		# raise()
	##
	def __iter__(self):
		worker_info = torch.utils.data.get_worker_info()
		if worker_info is None:  # single-process data loading, return the full iterator
			files = self.files
		else: # in a worker process, so split workload
			worker_id = int(worker_info.id)
			per_worker = np.ceil(len(self.files) / worker_info.num_workers).astype(np.int32)
			files = self.files[(worker_id*per_worker) : np.minimum(len(self.files), per_worker + (worker_id*per_worker)).astype(np.int32) ]
			# print('',flush=True)
			# print('Worker {}/{}, per worker: {}. '.format(worker_id,worker_info.num_workers,per_worker),end='')
			# print('Files:',files)
		#
		# return an iterator, striped across files. if train, striped across randomized subblock iterators of each file. else, each iter returns a full volume.
		file_iters = [self.reader(h5, **self.reader_kwargs) for h5 in files]
		# print('# of File Iters:', len(file_iters), flush=True)
		if self.train:
			sample_iter = roundrobin(*file_iters)
		else:
			sample_iter = itertools.chain(*file_iters)
		#
		return sample_iter
	# 
##

def IoU_flat(y: torch.Tensor, yhat: torch.Tensor, smooth=1e-6, ignore_background=False):
	'''
		batch x channels x height x width x depth for 3D
	'''
	# pdb.set_trace()
	ious = []
	uniq = torch.unique(y)
	for kidx, k in enumerate(uniq):
		if ignore_background and kidx==0:
			if uniq.shape[0] == 1: ious.append(0.0) # ensures the stack op works at the end
			continue
		#
		y_k = y==k
		yhat_k = yhat==k
		intersection = (yhat_k & y_k).float().sum((1, 2, 3, 4))
		union = (yhat_k | y_k).float().sum((1, 2, 3, 4)) 
		iou = (intersection + smooth) / (union + smooth)  # We smooth our devision to avoid 0/0
		thresholded = torch.clamp(20 * (iou - 0.5), 0, 10).ceil() / 10  # This is equal to comparing with thresolds
		ious.append(thresholded)
	#
	return torch.stack(ious,axis=-1)
##

# def IoU_nhot(y: torch.Tensor, yhat: torch.Tensor, smooth=1e-6):
def IoU_nhot(y, yhat, _numpy=False, smooth=1e-6):
	'''
		IoU using N-hot encoded representation.

		batch x N x height x width x depth for 3D
	'''
	# pdb.set_trace()
	ious = []
	for kidx in range(y.shape[1]): # 0 is the batch dimension, 1 is the N-hot (encoded) dimension
		y_k = (y[:,kidx,...] > 0.5)
		yhat_k = (yhat[:,kidx,...]  > 0.5)
		sum_axes = tuple(range(1,len(y_k.shape))) # 0 is still the batch dimension
		intersection = (yhat_k & y_k).float().sum(sum_axes) if not _numpy else (yhat_k & y_k).sum(sum_axes).astype(np.float64)
		union = (yhat_k | y_k).float().sum(sum_axes)  if not _numpy else (yhat_k | y_k).sum(sum_axes).astype(np.float64)
		iou = (intersection + smooth) / (union + smooth)  # We smooth our devision to avoid 0/0
		# thresholded = torch.clamp(20 * (iou - 0.5), 0, 10).ceil() / 10  # This is equal to comparing with thresolds
		# ious.append(thresholded)
		ious.append(iou)
	#
	return torch.stack(ious,axis=-1) if not _numpy else np.stack(ious,axis=-1)
##

def histmask(x, bins, pdf=True):
	''' Return boolean indices for points in x in between bins 
		* Works for Pytorch and Numpy

		If pdf==True: (PDF)
			then bins is used to define the bounds as: [bins[c-1], bins[c]) 
		Elif pdf==False: (PMF)
			then bins is used to define exact values
	'''
	if pdf:
		groups =  [((x >= bins[c-1]) * (x < bins[c])) for c in range(1,bins.shape[0])]
	else:
		groups = [(x==bins[c]) for c in range(0,bins.shape[0])]
	#
	incidence = [k.sum() for k in groups]
	if incidence[0].dtype==torch.int64:
		incidence = torch.stack(incidence).float()
	else:
		incidence = np.stack(incidence).astype(np.float64)
	#
	return groups, incidence
##

def totalVariation3D(y):
	''' pytorch '''
	bsize, chan, height, width, depth = y.size()
	dx0 = y[:,:,1:,:,:]
	dx1 = y[:,:,:-1,:,:]
	dy0 = y[:,:,:,1:,:]
	dy1 = y[:,:,:,:-1,:]
	dz0 = y[:,:,:,:,1:]
	dz1 = y[:,:,:,:,:-1]
##

def SSIM3D(y,yhat, **kwargs):
	return pytorchSSIM3D.ssim(y,yhat, **kwargs)
##

def dice_score(x, y, axes=(1,2,3,4), eps=0.001):
	xy_sum = (x*y).sum(axis=axes)
	x_sum = (x**2).sum(axis=axes)
	y_sum = (y**2).sum(axis=axes)
	dice = (2*xy_sum + eps) / ( x_sum + y_sum + eps)
	return dice
#

def getWeightGrads(model):
	grads = []
	for param in model.parameters():
		grads.append(param.grad.view(-1))
	grads = torch.cat(grads)
	return grads
	print(grads.shape)
##

def normalize_by_incidence(incidence):
	percent = incidence / incidence.sum()
	weight = (1.0/percent) / (1.0/percent).sum()
	# weight = (1.0 - percent) / (1.0 - percent).sum()
	return percent, weight
	''' Test:
	x = np.asarray([5000, 3000, 2000])
	y1 = x / x.sum()
	y2 = (1/y1) / (1/y1.sum())
	y3 = (1-y1) / (1 - y1.sum())
	np.all(x * y2 == (x*y2)[0])
	np.all(x * y3 == (x*y3)[0])
	'''
##



### ------------- LOSS SAMPLER ------------- ###

class lossSampler(torch.utils.data.sampler.Sampler):
	def __init__(self, df, batch_size, init_loss=10000):
		self.df = df
		self.batch_size = batch_size
		self.init_loss = init_loss

		self.gen = self.gen_sample()#self.df)

		self.__preprocessdf__()
		
	def __preprocessdf__(self):
		if 'sample_loss' not in self.df.columns:
			self.df['sample_loss'] = np.full(len(self.df), self.init_loss)
			self.df['sample_probs'] = np.full(len(self.df), 1/len(self.df))
			self.df['iters_since_sampled'] = np.zeros(len(self.df))
			self.df['times_sampled'] = np.zeros(len(self.df))
			self.df['sample_rate'] = np.zeros(len(self.df))

	def __len__(self):
		return len(self.df)

	def __iter__(self):
		return self.gen

	## UPDATES VARIOUS INTRA-SAMPLING METRICS
	def update_sample_metrics(self, idx, iter_cnt):
		self.df['iters_since_sampled'] += 1
		self.df.at[idx, 'iters_since_sampled'] = 0
		self.df.at[idx, 'times_sampled'] += 1
		self.df.at[idx, 'sample_rate'] = self.df.at[idx, 'times_sampled'] / (iter_cnt+1)

		if idx == self.maxp_sample[0]: # sampled the max period one
			self.maxp_sample = self.get_maxp_sample()
		else:
			self.maxp_sample = (self.maxp_sample[0], self.maxp_sample[1]+1) # increment

	## GETS THE SAMPLE WITH THE HIGHEST PERIOD 
	def get_maxp_sample(self):
		idx = self.df['iters_since_sampled'].argmax()
		return (idx, self.df.at[idx, 'iters_since_sampled']) # index of sample, period value
	
	## GENERATES THE SAMPLING PROBABILITIES FOR THE DF
	def gen_probs(self, sample_method='exploit'):
		if sample_method=='exploit':
			up_weighted = self.df['sample_loss']**2
			probs = up_weighted / sum(up_weighted)

		elif sample_method=='explore': 
			inverse_rate = 1 - self.df['sample_rate']
			probs = inverse_rate / sum(inverse_rate)
			
		else: # uniform distribution
			probs = np.full(len(self.df), 1/len(self.df))

		probs = np.array(probs) # renormalize in case they dont add to 1 b/c rounding error
		probs /= probs.sum()
		# probs.sum()
		self.df['sample_probs'] = probs
		return probs

	## GENERATES SAMPLES FOR DATALOADER
	def gen_sample(self, resample_prob_period=50, exploit_rate=0.85, max_period=1000):
		sample_cnt = 0 # how many samples have been generated
		iter_cnt = 0  # how many batches 
		self.maxp_sample = (0,0) # sample with the max period (i.e. hasnt been seen longest)

		while sample_cnt < len(self.df):
			# Resample probabilities 
			if iter_cnt % resample_prob_period == 0: 
			# if sample_cnt % resample_prob_period == 0: 
				if random.random() < exploit_rate: # decide whether to exploit or explore
					probs = self.gen_probs(sample_method='exploit')
				else:
					probs = self.gen_probs(sample_method='explore')

			# Make choice
			if self.maxp_sample[1] > max_period:
				idx = self.maxp_sample[0]
			else:
				idx = np.random.choice(self.df.index, p=probs)
			row = self.df.iloc[idx]

			# Update Sampling Metrics
			self.update_sample_metrics(idx, sample_cnt)
			sample_cnt += 1
			iter_cnt = int(sample_cnt/self.batch_size)

			yield (row['file'], row['idx'])

