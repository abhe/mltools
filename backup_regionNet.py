''' regionNet: classification of arbitrarily-sized regions
© Abhejit Rajagopal <abhejit.rajagopal@ucsf.edu>, 2020
'''
import os, sys, glob, time, random, itertools, warnings, pdb
from tqdm import tqdm
import numpy as np
import torch

# import hnn # no idea how to import this when mltools is a submodule?!

class dense_ReLU(torch.nn.Module):
	''' simple fully-connected relu network '''
	def __init__(self, in_channels, out_channels, width=10, depth=2, final=torch.nn.functional.softmax):
		super().__init__()
		self.in_channels = in_channels
		self.out_channels = out_channels
		self.depth = depth
		self.final_activation = final
		items = []
		for d in range(self.depth - 1):
			if d==0:
				items.append( torch.nn.Linear(self.in_channels, width, bias=True) )
			else:
				items.append( torch.nn.Linear(width, width, bias=True) )
			#
			items.append( torch.nn.ReLU(inplace=True) )
		#
		items.append( torch.nn.Linear(width, self.out_channels, bias=True ) )
		self.model = torch.nn.Sequential( *items )
	#
	def forward(self, x, activation='yes'):
		if activation is not None:
			return self.final_activation(self.model(x), dim=1)
		else:
			return self.model(x)
		#
	#
##

class DNN_ReLU(torch.nn.Module):
	''' simple fully-connected relu network '''
	def __init__(self, in_channels, out_channels, widths=(100), final_activation=True):
		super().__init__()
		self.in_channels = in_channels
		self.out_channels = out_channels
		self.widths = widths
		self.depth = len(self.widths) + 1
		# self.layers = []
		# cur_dim = self.in_channels
		# for l,w in enumerate(self.widths):
		# 	L = torch.nn.Linear(cur_dim, w)
		# 	self.layers.append(L)
		# 	self.layers.append(torch.nn.ReLU(inplace=True))
		# 	cur_dim = w
		# #
		# self.layers.append(torch.nn.Linear(cur_dim, out_channels)) # final layer
		# if final_activation: self.layers.append(torch.nn.ReLU(inplace=True))
		# self.model = torch.nn.Sequential( *self.layers )
		self.L0 = torch.nn.Linear(self.in_channels, self.widths[0])
		self.L1 = torch.nn.Linear(self.widths[0], self.widths[1])
		self.L2 = torch.nn.Linear(self.widths[1], self.widths[2])
		self.L3 = torch.nn.Linear(self.widths[2], self.widths[3])
		self.L4 = torch.nn.Linear(self.widths[3], self.out_channels)
		# self.activation = torch.relu
		self.activation = torch.nn.functional.leaky_relu
	#
	def forward(self, x):
		# return self.model(x)
		# cur_val = x
		# for l, L in enumerate(self.layers):
		# 	cur_val = torch.relu(L(cur_val))
		# #
		# return cur_val
		y0 = self.activation(self.L0(x))
		# y1 = self.activation(self.L1(y0))
		# y2 = self.activation((self.L2(y1))
		# y3 = self.activation((self.L3(y2))
		# y4 = self.L4(y3)
		y4 = self.L4(y0)
		return y4
	#
##

class dense_sigmoid(torch.nn.Module):
	''' simple fully-connected relu network '''
	def __init__(self, in_channels, out_channels, width=50, depth=4):
		super().__init__()
		self.in_channels = in_channels
		self.out_channels = out_channels
		self.width = width
		self.depth = depth
		# self.items = []
		# for d in range(self.depth - 1):
		# 	if d==0:
		# 		self.items.append( torch.nn.Linear(self.in_channels, self.width) )
		# 	else:
		# 		self.items.append( torch.nn.Linear( self.width, self.width) )
		# 	# items.append( torch.nn.ReLU(inplace=True) )
		# #
		# self.items.append( torch.nn.Linear( self.width, self.out_channels) )
		self.L0 = torch.nn.Linear(self.in_channels, self.width)
		self.L1 = torch.nn.Linear(self.width, self.width)
		self.L2 = torch.nn.Linear(self.width//2, self.width//2)
		self.L3 = torch.nn.Linear(self.width//2, self.width//2)
		self.L4 = torch.nn.Linear(self.width//2, self.out_channels)
	#
	def forward(self, x):
		# y = torch.sigmoid(self.items[0](x))
		# # for L in self.items[0:-1]:
		# # 	if y is None:
		# # 		y = L(x)
		# # 	else:
		# # 		y = L(y)
		# # 	#
		# # 	y = torch.sigmoid(y)
		# # #
		# y = self.items[-1](y)
		# pdb.set_trace()
		# y = torch.softmax(y, dim=1)
		# pdb.set_trace()
		y0 = torch.sigmoid(self.L0(x))
		y1 = torch.sigmoid(self.L1(y0))
		y2 = torch.sigmoid(self.L2(y1))
		# y2 = y1
		# y3 = torch.softmax(self.L3(y2),dim=1)
		y3 = self.L3(y2)
		return y3
	#
##

class regionNet(torch.nn.Module):
	''' This model takes an ND image+mask, and returns an approximate histogram+prediction for each region.
		--> num_filters and sigma_factor control the precision of the RBF-based histogram
		--> unique values in mask are taken to denote pixels corresponding to each region.
		--> if you only care about the histogram, can discard first model output.
	'''
	def __init__(self, out_channels, hmin=0.0, hmax=1.0, min_val=1, num_filters=24, sigma_factor=3, depth=2, dropout_factor=0.5):
		super().__init__()
		self.min = hmin
		self.max = hmax
		self.min_val = min_val
		self.num_filters = num_filters
		self.sigma_factor = sigma_factor
		self.hnn = hnn.histograNN(hmin=self.min, hmax=self.max, num_filters=self.num_filters, sigma_factor=self.sigma_factor)
		self.out_channels = out_channels # number of output values to predict for each input histogram
		self.depth = depth
		self.dropout_factor = dropout_factor
		items = []
		for d in range(self.depth - 1):
			if d==0:
				items.append( torch.nn.Linear(self.num_filters, 5, bias=True) )
			else:
				items.append( torch.nn.Linear(5, 5, bias=True) )
			#
			items.append( torch.nn.ReLU(inplace=True) )
			# items.append( torch.nn.Dropout(p=self.dropout_factor, inplace=True) )
		#
		items.append( torch.nn.Linear(5, self.out_channels, bias=True ) )
		# items.append( torch.nn.Sigmoid(inplace=True) )
		self.model = torch.nn.Sequential( *items )
	#
	def forward(self, x, masks,regions=None):
		mval = torch.unique(masks)
		mval = mval[mval>=self.min_val]
		hists = []
		for n in range(x.shape[0]):
			maskn = masks[n]
			groups, incidence = hnn.histmask(maskn, bins=mval, pdf=False)
			histn = []
			for g,i in zip(groups,incidence):
				if i==0: continue
				vals = x[n][g][None,...]
				hist = self.hnn(vals)
				histn.append(hist)
			#
			if len(histn)==0: continue
			histn = torch.cat(histn,axis=0)
			hists.append(histn)
		#
		preds = []
		for hist in hists:
			predn = torch.nn.functional.softmax(self.model(hist),dim=1)
			preds.append(predn)
		#
		return preds, hists # list, for each item in batch: [region x prediction_vec] & [region x histogram_vals]. Can't make into a 3dim tensor, because # of regions may not match for each example in batch.
	#
##